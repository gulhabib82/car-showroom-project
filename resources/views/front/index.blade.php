<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.101.0">
    <title>Car Showroom</title>

        <!-- Bootstrap core CSS -->

        <link rel="stylesheet" href="{{ asset('/dist/bootstrap4/css/bootstrap.min.css') }}">



        <style>
          .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
          }
    
          @media (min-width: 768px) {
            .bd-placeholder-img-lg {
              font-size: 3.5rem;
            }
          }
          .main-footer  
          {
            width: 100%;
            text-align: center;
            direction: rtl;
            margin-top: 30px;
            padding-top: 30px;
            padding-bottom:30px;
            border-top: 1px solid lightgrey;
            
          }
        </style>
    <body>
        
    
    
        <!-- Custom styles for this template -->
        <link href="{{ asset('/dist/bootstrap4/css/front.css') }}" rel="stylesheet">
  </head>

    @include('layouts.footer')
</body>
</html>
