<footer>
    <div class="footer-top">
        <div class="container">
            <ul>
                       <li><img style="    margin-right: 4px;" src="/static/images/icon-tel.png" alt="">(+971) 4 55 30 951</li>
                    <li><img style="    margin-right: 4px;" src="/static/images/icon-mobile.png" alt=""> (+971) 55 51 69 004</li>
                    <li><img style="    margin-right: 4px;" src="/static/images/icon-fax.png" alt="">(+971) 4 55 30 941</li>




            </ul>
        </div>
    </div>

    <div class="footer-content">
        <div class="container">
            <div class="row">





              
              
                 <div class="col-md-6 col-lg-3">
                    <h2 style="        font-size: 0.94rem;">CAR RENTAL DUBAI :</h2>
                    <ul>
                       
                          <li><a href="http://www.avantgarde.rentals/en/car-rental-dubai"><i class="fa fa-angle-right" aria-hidden="true"></i>Car Rental</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/promotions"><i class="fa fa-angle-right" aria-hidden="true"></i>Promotion Car Rental</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/vehicules/intermediate"><i class="fa fa-angle-right" aria-hidden="true"></i>Cheapest Car Rental</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/vehicules/premium"><i class="fa fa-angle-right" aria-hidden="true"></i>Rent a Car Dubai Airport</a></li>
                         
                    </ul>
                  </div>
             
                 <div class="col-md-6 col-lg-3">
                    <h2 style="        font-size: 0.94rem;">CAR HIRE DUBAI :</h2>
                    <ul>
                       
                          <li><a href="http://www.avantgarde.rentals/en/car-rental-dubai"><i class="fa fa-angle-right" aria-hidden="true"></i>Car Hire</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/about-us"><i class="fa fa-angle-right" aria-hidden="true"></i>Cheap Car Hire</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/quote-request"><i class="fa fa-angle-right" aria-hidden="true"></i>Hire Car Dubai Airport</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/promotions/low-cost-car-hire/8"><i class="fa fa-angle-right" aria-hidden="true"></i>Car Hire Cheap Dubai Airport</a></li>
                         
                    </ul>
                  </div>
             
                 <div class="col-md-6 col-lg-3">
                    <h2 style="        font-size: 0.94rem;">Long Term Car Rental Dubai</h2>
                    <ul>
                       
                          <li><a href="http://www.avantgarde.rentals/en/professionnels/2"><i class="fa fa-angle-right" aria-hidden="true"></i>Yearly Car Rental</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/professionals/long-term-rental-cars-in-the-united-arab-emirates/0"><i class="fa fa-angle-right" aria-hidden="true"></i>Car Lease</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/promotions/best-car-rental-deal/6"><i class="fa fa-angle-right" aria-hidden="true"></i>Monthly Car Rental</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/promotions/the-best-suv-for-rent/7"><i class="fa fa-angle-right" aria-hidden="true"></i>Long terrm car rental</a></li>
                         
                    </ul>
                  </div>
             
                 <div class="col-md-6 col-lg-3">
                    <h2 style="        font-size: 0.94rem;">Luxury Car Rental Dubai</h2>
                    <ul>
                       
                          <li><a href="http://www.avantgarde.rentals/en/vehicules/luxury"><i class="fa fa-angle-right" aria-hidden="true"></i>Luxury Car Rental In Dubai</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/vehicule/529491e534bfe2736fbd461a"><i class="fa fa-angle-right" aria-hidden="true"></i>Rent a Luxury Car</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/vehicule/534fea63efaccc5b10d79f09"><i class="fa fa-angle-right" aria-hidden="true"></i>Hire a Luxury Audi in dubai</a></li>
                         
                          <li><a href="http://www.avantgarde.rentals/en/vehicule/53510e42efaccc709d4647ef"><i class="fa fa-angle-right" aria-hidden="true"></i>Rent a Ferrari Dubai Airport</a></li>
                         
                    </ul>
                  </div>
             

             


            </div>
        </div>
    </div>

    <div class="footer-menu">
        <div class="container">
            <div class="d-lg-flex">
                <div class="">
                    <a href="#"><img src="/static/assets/images/logo-footer.png" alt="Avantgarde"></a>
                </div>
                <div class="footer-nav-menu">
                    <ul>
                            <li><a class="" href="/">Home</a></li>
                            <li><a class="hidden_mobile " href="/en/reservation-voiture-tunisie">Service</a></li>
                            <li class="hidden_mobile"><a href="/en/contactez-nous" class="">Contact-us</a></li>




                            <li><a class="" href="/en/privacy-policy">Privacy policy</a></li>






                            <li><a class="" href="/en/conditions-de-location">Rental terms &amp; conditions</a></li>

                            <li><a class="" href="/en/faq">FAQ</a></li>
                            <li class="hidden_mobile"><a class="" href="/en/sitemap">Site map</a></li>

                    </ul>
                </div>
                <div class="rs-icons d-flex">
                    <a href="https://www.instagram.com/avantgarde.rentals/"><i class="fa fa-instagram" aria-hidden="true"></i></a>
                    <a href="skype:avantgarde.rentals"><i class="fa fa-skype" aria-hidden="true"></i></a>
                    <a href="https://www.facebook.com/avantgarde.rentals/"><i class="fa fa-facebook" aria-hidden="true"></i></a>
                </div>
            </div>


            <div class="d-lg-flex copyright">
              <div class="">
                    <a href="http://www.smart2dotech.com"><img style=" margin-left: 32px;   width: 62px;" src="/static/assets/images/smart2do.png" alt="Smart2do Technologies"></a>
                </div>

                <div class="footer-nav-menu">
                    <ul>
                            <li><a style="    font-size: 10px;" class="" href="http://www.smart2dotech.com">©2019 AVANTGARDE . All rights reserved. designed and maintained by  Smart2Do Technologies</a></li>


                    </ul>
                </div>
            </div>



        </div>
    </div>








</footer>